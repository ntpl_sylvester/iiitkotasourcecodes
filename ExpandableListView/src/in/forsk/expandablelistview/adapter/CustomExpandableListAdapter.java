package in.forsk.expandablelistview.adapter;

import in.forsk.expandablelistview.R;
import in.forsk.expandablelistview.wrapper.FacultyWrapper;

import java.util.ArrayList;
import java.util.HashMap;

import com.androidquery.AQuery;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomExpandableListAdapter extends BaseExpandableListAdapter {
	private final static String TAG = CustomExpandableListAdapter.class.getSimpleName();
	Context context;

	// to hold the data model (Instance variable )
	// as best practice ,data which is coming from an outside scope by
	// method/constructors
	// need to be hold for the lifetime of the class object
	// This will store unique department name
	ArrayList<String> headerData;

	// This hash map will hold list of faculty for every unique department
	HashMap<String, ArrayList<FacultyWrapper>> ChildListData;
	AQuery aq;

	LayoutInflater inflater;

	public CustomExpandableListAdapter(Context context, ArrayList<String> headerData, HashMap<String, ArrayList<FacultyWrapper>> ChildListData) {
		// TODO Auto-generated constructor stub
		this.context = context;
		this.headerData = headerData;
		this.ChildListData = ChildListData;

		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		aq = new AQuery(context);
	}

	// Return header count
	@Override
	public int getGroupCount() {
		return headerData.size();
	}

	// This will return child count for particular group index
	@Override
	public int getChildrenCount(int groupPosition) {
		return ChildListData.get(headerData.get(groupPosition)).size();
	}

	// return group object at a particular index
	@Override
	public Object getGroup(int groupPosition) {
		return headerData.get(groupPosition);
	}

	// return child object at a particular index
	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return ChildListData.get(headerData.get(groupPosition)).get(childPosition);
	}

	// Return group id
	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	// Return child id
	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	// Get view is not implemented in expandable list view
	// since there are two views (Group view and child View)
	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.list_group, null);
		}

		String title = (String) getGroup(groupPosition);

		TextView tv = (TextView) convertView.findViewById(R.id.listTitle);
		tv.setText(title);
		return convertView;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
		ViewHoder holder;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.row_faculty_profile_list, null);
			Log.d(TAG, "New View init - " + childPosition);

			// well set up the ViewHolder
			holder = new ViewHoder(convertView);

			// store the holder with the view.
			convertView.setTag(holder);

		} else {
			Log.d(TAG, "Recycling old Views - " + childPosition);

			// we've just avoided calling findViewById() on resource everytime
			// just use the viewHolder
			holder = (ViewHoder) convertView.getTag();

		}

		// Retrieve the data at particular index(position)
		// so we can bind the correct data
		// FacultyWrapper obj = mFacultyDataList.get(position);
		FacultyWrapper obj = (FacultyWrapper) getChild(groupPosition, childPosition);

		// Now we are accessing same object via holder
		// As now we are not using findviewbyId so we don't need to traverse
		// view hierarchy
		// this will increase the list view performance
		AQuery temp_aq = aq.recycle(convertView);
		temp_aq.id(holder.profileIv).image(obj.getPhoto(), true, true, 200, 0);

		holder.nameTv.setText(obj.getFirst_name() + " " + obj.getLast_name());
		holder.departmentTv.setText(obj.getDepartment());
		holder.reserch_areaTv.setText(obj.getReserch_area());

		Log.d(TAG, "get View - " + childPosition);

		return convertView;
	}

	// The ViewHolder design pattern enables you to access each list item view
	// without the need for the look up,
	// saving valuable processor cycles. Specifically, it avoids frequent call
	// of findViewById() during ListView
	// scrolling, and that will make it smooth.
	// http://www.javacodegeeks.com/2013/09/android-viewholder-pattern-example.html
	public static class ViewHoder {
		ImageView profileIv;
		TextView nameTv, departmentTv, reserch_areaTv;

		public ViewHoder(View view) {
			profileIv = (ImageView) view.findViewById(R.id.profileIv);

			nameTv = (TextView) view.findViewById(R.id.nameTv);
			departmentTv = (TextView) view.findViewById(R.id.departmentTv);
			reserch_areaTv = (TextView) view.findViewById(R.id.reserch_areaTv);
		}
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

}
